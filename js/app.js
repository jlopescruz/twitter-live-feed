/* Google Maps */
var map;

function initialize() {
  var myLatlng = new google.maps.LatLng(51.5072, 0.1275);
  var mapOptions = {
    zoom: 4,
    center: myLatlng,
    disableDefaultUI: true
  }

  map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);
}

google.maps.event.addDomListener(window, 'load', initialize);

/* AngularJS App */
var app = angular.module('plunker', ['firebase']);

app.controller('MainCtrl', function($scope, $firebase) {
    $scope.messages = {};
  
    var ref    = new Firebase("https://the-space.firebaseio.com/mentions"),
        sync   = $firebase(ref);
       
    $scope.tweets = sync.$asObject();
    ref.on('child_added', function(data) {
        data = data.val();

        var curMarker = new google.maps.Marker({
            position: new google.maps.LatLng(data.lat, data.lng),
            map: map,
            icon: 'images/pin10.svg'
        });

        var infoMarkup = '';
        
        infoMarkup += '<div class="info-markup">';
        infoMarkup += '<img src="' + data.image +'" alt="">';
        infoMarkup += '<h1>' + data.user +' - <a target="_blank" href="http://twitter.com/' + data.screenname +'">@'+ data.screenname +'</a></h1>';
        infoMarkup += '<p>' + data.text +'</p>';
        infoMarkup += '</div>';


        var infowindow = new google.maps.InfoWindow({
            content: infoMarkup
        });

        google.maps.event.addListener(curMarker, 'click', function() { 
            infowindow.open(map,curMarker); 
        });

        //map.setCenter(new google.maps.LatLng(data.lat, data.lng));
    });
});
