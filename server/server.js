/* Twitter API Keys */
/* https://www.npmjs.org/package/twitter */

var api_key              = 'H6fJrn0DqYCjSLUmKlhVMNZvX',
    api_secret           = 'MKdYYMPVoW9jgsXfnGXXs0wQNJNooYe88kFJWpdh1TMiZFyuAK',
    access_token         = '20113802-mIvaPvGB5VrX1KBUVsmvYDRjFb2QZtS7ppinRTSDu',
    access_token_secret  = '5Zf6gtRZsxMAnuWCaPv2oaL3QWmSYDI0RCSGu6RXUFw4h';

/* Google Maps API Key */
var google_key           = 'AIzaSyBlAnnOAkmjmcTklluCjl_wSpM6UUIGp3g';

var twitter              = require('twitter'),
    Firebase             = require('firebase'),
    json_request         = require('request');

var twit                 = new twitter({
        consumer_key:        api_key,
        consumer_secret:     api_secret,
        access_token_key:    access_token,
        access_token_secret: access_token_secret
});

/* Firebase Access */
var myFirebaseRef        = new Firebase("https://the-space.firebaseio.com/mentions");

/* Twitter keywords */
var KEYWORDS             = ['thespacearts', 'thespace'];

twit.stream('statuses/filter', {track:KEYWORDS}, function(stream) {
    
    stream.on('data', function(data) {
        if (data.user.location !== '') {

            /* Get coordinates based on location */
            json_request({
                url: 'https://maps.googleapis.com/maps/api/geocode/json?address=' + data.user.location +'&key=' + google_key,
                json: true
            }, function(error, response, body) {
                if (!error && response.statusCode === 200 && body.results[0]) {
                    /* Store data into Firebase */
                    myFirebaseRef.push({
                        user:        data.user.name,
                        text:        data.text,
                        image:       data.user.profile_image_url,
                        screenname:  data.user.screen_name,
                        location:    data.user.location,
                        lat:         body.results[0].geometry.location.lat,
                        lng:         body.results[0].geometry.location.lng
                    });

                    console.log(data.user.name + ' - ' + data.user.location);
                    console.log(data.text);
                    console.log('Tweet added');
                    console.log('');
                }
            });
        }
        
    });
    
    // Disconnect stream after five seconds
    //setTimeout(stream.destroy, 5000);
});

console.log('App is listening...');