Twitter live feed
=================

This code is part of the task:
https://the-space.atlassian.net/browse/SWD-841

The aim was to create a realtime app using new technologies such as NodeJS, AngularJS and Firebase and display a globe map with the location of each tweet that mentions thespacearts or any other keyword.

Installation
------------

npm install


Run Node Server
---------------

- cd /server
- node server.js


Dependencies
------------
This project is using:

- NodeJS
- AngularJS
- AngularFire
- Firebase
- Google Maps API
- Google Address API
